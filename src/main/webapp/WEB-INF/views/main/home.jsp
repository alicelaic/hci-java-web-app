<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html>

<head>
    <title>LIP - hci</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta http-equiv="Cache-Control" content="no-store"/>
    <meta charset="UTF-8">


    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/resources/css/header/bootstrap.min.css"/>
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/resources/css/jquery.tagsinput.css"/>
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/resources/css/login.css"/>
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/resources/css/lip.css"/>
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/resources/css/header/sb-admin.css"/>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link href="${pageContext.request.contextPath}/resources/css/interactiveExamples.css" rel="stylesheet"
          type="text/css">
    <link href="${pageContext.request.contextPath}/resources/css/bowlExample.css" rel="stylesheet"
          type="text/css">
    <link href="${pageContext.request.contextPath}/resources/css/inheritance.css" rel="stylesheet"
          type="text/css">

    <!-- JS-->
    <script type="text/javascript" src="//code.jquery.com/jquery-1.9.1.js"></script>
    <script type="text/javascript"
            src="//maxcdn.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/resources/js/jquery.tagsinput.js"></script>
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/resources/js/leapMotion/leap-0.6.4.js"></script>
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/resources/js/leapMotion/leap-plugins-0.1.10.js"></script>
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/resources/js/three.min.js"></script>
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/resources/js/OrbitControls.js"></script>
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/resources/js/leap.pinchEvent.js"></script>
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/resources/js/leap-plugins-0.1.11pre.js"></script>
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/resources/js/sigma.min.js"></script>
</head>

<body>
<div id="wrapper">
    <tiles:insertAttribute name="header1" ignore="true"/>

    <tiles:insertAttribute name="body"/>


    <tiles:insertAttribute name="footer" ignore="true"/>

</div>
</body>
</html>


