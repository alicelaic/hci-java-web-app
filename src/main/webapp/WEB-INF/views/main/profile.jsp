<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<div class="container" style="margin-top: 40px;">
    <div class="row">
        <div class="col-lg-12">
            <h3>My Profile</h3>

            <p>You can find the badges that you earned during your experience on this platform and the progress of what you've completed: </p>
        </div>
    </div>
    <div class="row">
        <div class="progress-wrapper">
            <div class="progress" style="width: ${progress}%">${progress}% completed</div>
        </div>
    </div>

    <!-- Projects Row -->
    <div class="row">

        <c:forEach var="badge" items="${badges}">
            <div class="col-md-3 portfolio-item">
                <img style="height:200px;width:300px;" class="img-responsive" src="${badge.badgeType.source}" alt="">

                <div><p style="text-align:center">${badge.name}</p></div>
            </div>
        </c:forEach>

    </div>
    <!-- /.row -->
</div>



