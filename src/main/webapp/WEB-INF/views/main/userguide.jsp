<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<div class="container" style="margin-top: 40px;">
  <div class="row">
    <div class="col-lg-12">
      <h3>User guide</h3>
    </div>
  </div>

  <!-- Projects Row -->
  <div class="row">


        <div style="border:1px solid;">
          <img class="img-responsive" src="/resources/img/home.png" alt="">

          <div><p style="color:#E64D48">The chapters that are completed are enabled and you have access from all the information from there.</p></div>
        </div>

       <div style="border:1px solid">
         <img class="img-responsive" src="/resources/img/storyboard_example.png" alt="">
         <div><p style="color:#E64D48">There are lessons where you have an interactive example which works only with leap motion (click on the icon to see it).</p></div>
       </div>

       <div style="border:1px solid">
         <img class="img-responsive" src="/resources/img/storyboard_web.png" alt="">
         <div><p style="color:#E64D48">When you open an interactive example first you have some indications, follow them and complete the example.</p></div>
      </div>

      <div style="border:1px solid">
        <img class="img-responsive" src="/resources/img/storyboard_medal.png" alt="">
        <div><p style="color:#E64D48">When you finsish the example you can take your medal.</p></div>
      </div>

    <div style="border:1px solid">
      <img class="img-responsive" src="/resources/img/storyboard_test.png" alt="">
      <div><p style="color:#E64D48">At the end of a chapter you have to take a test in order to unlock the next chapter</p></div>
    </div>

    <div style="border:1px solid">
      <img class="img-responsive" src="/resources/img/storyboard_profile.png" alt="">
      <div><p style="color:#E64D48">On your profile page you can see your progress and your badges.</p></div>
    </div>


  </div>
  <!-- /.row -->
</div>



