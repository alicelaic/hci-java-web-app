<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/views/fragments/verticalMenu.jsp"%>
<div id="page-wrapper">

    <div class="container-fluid">
        <div class="row inside test">
            <form action="submit-question" method="POST">
                <div class="question-title">${question.text}</div>

                <div class="answers">
                    <ul>
                        <li><input type="radio" name="answer" value="1" checked/><span>${question.answer1}</span></li>
                        <li><input type="radio" name="answer" value="2"/><span>${question.answer2}</span></li>
                        <li><input type="radio" name="answer" value="3"/><span>${question.answer3}</span></li>
                    </ul>
                </div>
                <div><input type="submit" value="Submit question"/></div>
            </form>
        </div>

        <div id="exampleDiv" class="hidden">
        </div>
    </div>

</div>