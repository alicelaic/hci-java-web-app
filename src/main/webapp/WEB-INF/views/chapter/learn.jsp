<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/views/fragments/verticalMenu.jsp"%>
<div id="page-wrapper">

    <div class="container-fluid">
        <div class="row inside">
            <c:forEach var="paragraph" items="${paragraphs}">
                <div class="col-lg-12">
                    ${paragraph.description}
                </div>
            </c:forEach>



            <script type="text/javascript"
                    src="${pageContext.request.contextPath}/resources/js/interactiveExamples.js"></script>

            <div class="navigation-buttons">
                <form action="prev" method="post">
                    <div class="prev">
                        <input type="submit" value="< Previous"/>
                    </div>
                </form>
                <form action="next" method="post">
                    <div class="next">
                        <input type="submit" value="Next >"/>
                    </div>
                </form>
            </div>
        </div>

        <div id="exampleDiv" class="hidden"></div>

  <%--      <c:if test="${finish}">
            <div id="exampleDiv" class="overlay">
                <img src="/resources/img/stick-figure-black.jpg" />
            </div>
        </c:if>--%>
    </div>

</div>