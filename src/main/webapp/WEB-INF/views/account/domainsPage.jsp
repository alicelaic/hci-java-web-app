<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="container-fluid" style="margin: 20%;">
    <div class="row inside">
        <div class="domain-choose">
            <div class="col-lg-12">
                <h1>LIP (Learn Interactive Programming)</h1>
            </div>

            <div class="domain-content">
                <p>Please choose your domain of interest:</p>

                <form action="welcome" method="post">
                    <div>
                        <c:forEach var="domainVar" items="${domains}">
                            <input type="submit" name="domain" value="${domainVar.name}"/>
                        </c:forEach>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>