<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<link rel="stylesheet" href="<c:url value="/resources/css/custom.css"/>"/>
<script src="<c:url value='/resources/js/jquery.blockUI.js'/>"></script>
<%--<form method="post" action="${pageContext.request.contextPath}/image/previous">
      <input type="submit" id="previousBtn"
             value="<" class="btn btn-success"/>
  </form>--%>
<div style="width: 50%; margin-right: auto; text-align: center; display: inline-flex; margin-left: 50%; margin-bottom: 10px;">


    <form method="post" action="${pageContext.request.contextPath}/image/next">
        <input type="submit" id="nextBtn"
               value=">" class="btn btn-success" style="margin-left: 5px;"/>
    </form>
</div>
<c:forEach var="i" begin="0" end="${fn:length(images) -1}">
    <c:choose>
        <c:when test="i%3 = 0">
            <div class="row">
        </c:when>
        <c:when test="i%3 = 2">
            </div>
        </c:when>
    </c:choose>

    <div style="border: 1px solid grey;"  class="col-md-4 portfolio-item" id-image="${images[i].id }">
        <c:set var="tagsName" value=""/>
        <c:forEach items="${images[i].tagsSelected}" var="tag">
            <c:set var="tagsName" value="${tag.tag.name},, ${tagsName }"/>
        </c:forEach>
        <a> <img data-id="${tagsName }" class="img-responsive"
                                                     id="${images[i].id }" src="${images[i].link }">
        </a>

        <p>
            <a>Tags associated with this image: </a>
        </p>
        <p style="height:50px">
            <c:forEach items="${images[i].tagsSelected}" var="tag" >
                <input  type="checkbox" id-image="${images[i].id}" value="${tag.tag.name}" class="selectedTags" checked>  ${tag.tag.name}
            </c:forEach>
        </p>

        <p style="color:blue">Add your own tags:</p>

       <input id="ownTags" id-image="${images[i].id}" class="ownTags" type="text" placeholder="Add your own tags"/>

            <p style="color:blue">Add sentiments for this image:</p>
            <form>
                <input type="radio" name="likes" id="sentiment1" class="likes" id-value="1">Strongly like<br>
                <input type="radio" name="likes" id="sentiment2" class="likes" id-value="2">Like more than medium </br>
                <input type="radio" name="likes" id="sentiment3" class="likes" id-value="3">Like medium </br>
                <input type="radio" name="likes" id="sentiment4" class="likes" id-value="4">Almost like </br>
                <input type="radio" name="likes" id="sentiment5" class="likes" id-value="5">Don't like it all </br>
            </form>

        <form>
        <button id="id1" name="id1" id-image="${images[i].id }" type="button" class="btn btn-default id1">Submit your changes</button>
        </form>
    </div>

</c:forEach>
<script>
    (function() {
        $('.ownTags').tagsInput();
    })();


</script>
<script>

    $(document).on('click','.id1',function () {
        $.blockUI({
            message : 'Please wait...'
        });
        var id = $(this).attr('id-image');
        $(this).attr('disabled', 'disabled');
        var userTags= $(".ownTags[id-image='"+id+"']").val();//document.getElementById("ownTags").value;
        console.log(userTags);

        var selectedTags ="";

        var imageTags = $('input[id-image="'+id+'"]:checked');//document.getElementsByClassName('selectedTags');
        for(var i=0; imageTags[i]; ++i){
            console.log(imageTags[i]);
            if(imageTags[i].checked){
                selectedTags+=imageTags[i].value+",";
            }
        }

        if(userTags.length>0) {
            selectedTags+=userTags;
        }
        else if(selectedTags.length>0) {
            selectedTags=selectedTags.substring(0,selectedTags.length-1);
        }

        var like;
        like = $(".likes:checked").attr("id-value");
        if(like === undefined){
            like = 3;
        }

        $.ajax({
            type: 'POST',
            url: '${pageContext.request.contextPath}/image/annotateImage',
            data: {
                imageId: id,
                selectedTags: selectedTags,
                like: like
            },
            success: function (data) {
                document.open();
                document.write(data);
                document.close();
                $.unblockUI;
            },
            error: function(data){
                document.open();
                document.write(data);
                document.close();
                $.unblockUI;

            }
        })
    });

</script>

