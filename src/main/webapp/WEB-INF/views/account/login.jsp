<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<div id="login">

    <sf:form method="POST" action="login">
        <div id="login-form">
            <input class="input-field input-username" name="j_username" type="text" placeholder="Your username..."/><br/>
            <input class="input-field" name="j_password" type="password" placeholder="Your password..."/><br/>
            <button id="submit-login" class="btn btn-default">Submit credentials</button>
        </div>
    </sf:form>
</div>