<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>

<script src="<c:url value='/resources/js/jquery.blockUI.js'/>"></script>
<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand nav-img">
            <img style="height:50px;width:50px;" class="img-responsive" src="resources/img/logo.jpg" alt="">

        </a>

    </div>


    <!-- Top Menu Items -->
    <ul class="nav navbar-right top-nav">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-leanpub"></i> <b
                    class="caret"></b></a>
            <ul class="dropdown-menu alert-dropdown">
                <c:forEach var="domain" items="${domains}">
                    <li><a href="domain?name=${domain.name}"><span class="label ${activeChapter.domain.name==domain.name ? 'label-danger' : 'label-default'}">${domain.name}</span>
                    </a>
                    </li>
                </c:forEach>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-lightbulb-o"></i> <b
                    class="caret"></b></a>
            <ul class="dropdown-menu alert-dropdown">
                <li>
                    <a href="chapter?name=${activeChapter.name}"><span class="label ${operationType == 'learn' ? 'label-danger' : 'label-default'}">Learn</span></a>
                </li>
                <li>
                    <a href="test?name=${activeChapter.name}"><span class="label ${operationType == 'test' ? 'label-danger' : 'label-default'}">Test</span></a>
                </li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i>${username} <b
                    class="caret"></b></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="profile"><i class="fa fa-fw fa-user"></i> Profile</a>
                </li>
                <li>
                    <a href="userguide"><i ></i> Userguide</a>
                </li>
               <%-- <li>
                    <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                </li>--%>
                <li class="divider"></li>
                <li>
                    <a href="logout"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                </li>
            </ul>
        </li>
    </ul>

</nav>