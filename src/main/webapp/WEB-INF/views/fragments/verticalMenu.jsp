<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="collapse navbar-collapse navbar-ex1-collapse">
  <ul class="nav navbar-nav side-nav">
    <c:forEach var="chapterStatus" items="${chaptersStatus}">
      <li class="${chapterStatus.chapter.name==activeChapter.name ? "active" : ""} ${chapterStatus.blocked==true ? "blocked" : ""}"> <a href="chapter?name=${chapterStatus.chapter.name}"><i class="fa fa-fw"><c:out value="${chapterStatus.chapter.name}"/></i> </a>
      </li>
    </c:forEach>

  </ul>
</div>