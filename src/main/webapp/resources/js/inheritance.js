window.figure=$('.base-figure');
window.canvas = $('#canvas');
window.explanationsParagraph = $('#explanationsParagraph');

var moveFigure = function (elem, screenPosition) {

    var leftPosition = screenPosition[0];
    var bottomPosition = screenPosition[1];

    if(figure[0].offsetLeft<canvas[0].offsetLeft) {
        return;
    }

    elem.css({
        left: leftPosition + 'px',
        bottom: bottomPosition + 'px'
    });

};

var isPinched =false;
var exampleCompleted=false;
// Set up plugins
Leap.loop()
//    .use('transform', {vr: true})
    .use('boneHand', {
        targetEl: document.body,
        arm: false,
        opacity: 0.8
    }).use('pinchEvent');
// Set up scene
var scene    = Leap.loopController.plugins.boneHand.scene;
var camera   = Leap.loopController.plugins.boneHand.camera;
var renderer = Leap.loopController.plugins.boneHand.renderer;
var plane = new THREE.Mesh(
    new THREE.PlaneGeometry(0,0),
    new THREE.MeshBasicMaterial({color: 0xFFFFFF, side: THREE.DoubleSide})
);
plane.scale.set(2, 2, 2);
plane.position.set(0, 400, -100);
plane.receiveShadow = true;
scene.add(plane);
camera.lookAt( plane.position );
/* var axisHelper = new THREE.AxisHelper( 100 );
 scene.add( axisHelper );*/
var controls = new THREE.OrbitControls( camera, renderer.domElement );
Leap.loopController.on('handFound', function(hand) {

    if(exampleCompleted===false){
        document.querySelector('canvas').style.display = 'block';
    }else{
        document.querySelector('canvas').style.display = 'none';
    }


}).on('handLost', function(hand){
    if (Leap.loopController.frame(0).hands.length === 0){
        document.querySelector('canvas').style.display = 'none';
    }
});

Leap.loopController.on(('hand'),function(hand) {

    if(isPinched)
    {
        moveFigure(figure,hand.stabilizedPalmPosition);
    }

});
window.testFigure = document.getElementById("canvas");

Leap.loopController.on('pinch', function (hand) {
    console.log("screenPosition: "+hand.stabilizedPalmPosition);
    if( hand.stabilizedPalmPosition[0]> 90 &&  hand.stabilizedPalmPosition[0]<120){

        isPinched=true;

        console.log("---------------------innn");
    }else{

        console.log("-----------------------ouuuut");
    }

}).on('unpinch', function (hand) {
    console.log('unpinch', hand);

    if(isPinched && !exampleCompleted) {
        isPinched=false;

        figure.css({
            left: 300 + 'px',
            bottom: 300 + 'px'
        });

        $('#first-child').fadeIn(6000);
        $('#second-child').fadeIn(6000);

        explanationsParagraph.html("<p>That's what is happening in inheritance: children classes are derived from a base class. ");

        explanationsParagraph.append("<br><b>Congratulations! You have completed this example!</b>"+"Press the button to complete <button onClick='completeStep()'>Complete</button> ");
        exampleCompleted=true;
    }

});