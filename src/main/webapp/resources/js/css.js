window.cursor = $('#cursor');
window.output = $('#output');
window.bowl = $('.bowl');
window.canvas = $('#canvas');
window.explanationsParagraph = $('#explanationsParagraph');
window.exampleDiv = $('#exampleDiv');
window.canvasDiv = $('#canvas');
window.textInsideBowl = $('#text-inside-bowl');

window.cssColor = $('.css-color');
window.cssFont = $('.css-font');
window.cssAlign = $('.css-align');
window.cssBold = $('.css-bold');
window.cssItalic = $('.css-italic');
window.cssBackgroundColor = $('.css-background-color');

var numberOfCssElements=1;
var isElemSelected = false;

var positionsInBowl = {'.css-color': {left: 135, bottom: 340},
    '.css-font': {left: 235, bottom: 340},
    '.css-align': {left: 435, bottom: 340},
    '.css-bold': {left: 145, bottom: 300},
    '.css-italic': {left: 123, bottom: 300},
    '.css-background-color': {left: 470, bottom: 300}
};

var elemsInBowl = {'.css-color': false,
    '.css-font': false,
    '.css-align': false,
    '.css-bold': false,
    '.css-italic': false,
    '.css-background-color': false
};

/* true if the element have been in bowl, even if it was removed from the bowl*/
var elemsPlacedInBowl = {'.css-color': false,
    '.css-font': false,
    '.css-align': false,
    '.css-bold': false,
    '.css-italic': false,
    '.css-background-color': false
};

var numberOfElemPlacedInBowl=0;

var touchLeft = function (objectLeft, objectWidth, cursorLeft, cursorWidth) {
    if ((cursorLeft + cursorWidth) >= objectLeft && cursorLeft <= (objectLeft + objectWidth)) {
        return true;
    }
    return false;
};

var touchTop = function (objectTop, objectHeight, cursorTop, cursorHeight) {
    if ((cursorTop + cursorHeight) >= objectTop && cursorTop <= (objectTop + objectHeight)) {
        return true;
    }
    return false;
};

/*the element movable in inside static content*/
var touchStaticObject = function (static, movable) {
    return touchLeft(static.offsetLeft, static.clientWidth, movable.offsetLeft, movable.clientWidth) &&
        touchTop(static.offsetTop, static.clientHeight, movable.offsetTop, movable.clientHeight);
};

var placeObjectInBowl = function (elem) {

    elemsInBowl[elem.selector] = true;

    if(elemsPlacedInBowl[elem.selector]==false)
    {
        numberOfElemPlacedInBowl++;
    }
    elemsPlacedInBowl[elem.selector]=true;

    elem.css({
        left: positionsInBowl[elem.selector].left + 'px',
        bottom: positionsInBowl[elem.selector].bottom - 200 + 'px',
        'opacity': '0.1'

    });
};

var removeElementFromBowl = function (elem) {

    elemsInBowl[elem.selector] = false;

    elem.css({
        'opacity': 'inherit'

    });

};

var moveObject = function (elem, screenPosition) {

    var leftPosition = screenPosition[0] * 5;
    var bottomPosition = screenPosition[1];

    if (leftPosition < canvas[0].offsetLeft) {
        leftPosition = canvas[0].offsetLeft;
    } else if (leftPosition >= canvas[0].offsetLeft + canvas[0].clientWidth) {
        leftPosition = canvas[0].offsetLeft + canvas[0].clientWidth;
    }

    if (bottomPosition > canvas[0].offsetTop + canvas[0].clientHeight) {
        bottomPosition = canvas[0].offsetTop + canvas[0].clientHeight;
    } else if (bottomPosition < canvas[0].offsetTop) {
        bottomPosition = canvas[0].offsetTop;
    }

    elem.css({
        left: leftPosition + 'px', /**5*/
        bottom: bottomPosition + 'px'
    });
};

var addProperty = function (property, value) {

    switch (property) {
        case 'background-color':
            //bowl[0].style.removeProperty('background-color');
            bowl[0].style[property] = value;

            break;

        case 'text-align':
            bowl[0].style[property] = value;
            break;
        default:
            textInsideBowl[0].style[property] = value;

    }

    explanationsParagraph.html("You just added the CSS property      <b>"+property+" : "+ value+"</b>");

    if(numberOfElemPlacedInBowl===numberOfCssElements)
    {
        $.ajax(
            {
                method: "GET",
                url: "checkBadge",
                data: {badgeName:"CSS Master"},
                success:function(data)
            {
                    console.log(data);
                   if(data!=="hasBadge")
                   {
/*
                      explanationsParagraph.append("<br><b>Congratulations! You have completed this example!</b>"+"Press the button to complete <form action='badge' method='post'><input name='badgeName' value='My super badge' hidden='true'><input type='submit' value='submit' /></form> ");
*/
                       explanationsParagraph.append("<br><b>Congratulations! You have completed this example!</b>"+"Press the button to complete <button onClick='completeStep()'>Complete</button> ");
                       controller.disconnect();
                   }
            }
            }
        )
    }
};

var removeProperty = function (property) {
    switch (property) {
        case 'background-color':
            //bowl[0].style.removeProperty('background-color');
            bowl[0].style[property] = 'initial';
            break;

        case 'text-align':
            bowl[0].style[property] = 'initial';
            break;
        default:
            textInsideBowl[0].style[property] = 'initial';
    }
};

var logicOnObjects = function (elem, frame, i, callToApply, removePropertyCallback) {
    var screenPosition = frame.pointables[i].tipPosition;

    if (touchStaticObject(elem[0], cursor[0])) {
        elem.css({
            'background-color': '#E64D48'
        });


        if (!isElemSelected && frame.pointables[i].touchZone === 'touching') {
            isElemSelected = true;
            moveObject(elem, screenPosition);
            // cssColor is inside bowl
            if (!elemsInBowl[elem.selector] && touchStaticObject(bowl[0], elem[0])) {

                elem.addClass('animate');
                elem.addClass('move');
                setTimeout(function () {
                    elem.removeClass('animate');
                    elem.removeClass('move');
                    callToApply();

                }, 1000);

                placeObjectInBowl(elem);
            } else if (touchStaticObject(bowl[0], elem[0])) {
                // cssColorInBowl = false;
                moveObject(elem, screenPosition);
            } else {


                removeElementFromBowl(elem);
                removePropertyCallback();
                moveObject(elem, screenPosition);
            }
        } else {
            isElemSelected = false;

        }

    } else {
        elem.css({
            'background-color': '#FF9999'
        });
    }
};


// Setup Leap loop with frame callback function
var controllerOptions = {enableGestures: true,background:false};
var removeStyleFromObject = function(property, val) {
    var value = val !== undefined ? val :  'initial';
    switch (property) {
        case 'background-color':
            //bowl[0].style.removeProperty('background-color');
            bowl[0].style[property] = value;
            break;

        case 'text-align':
            bowl[0].style[property] = value;
            break;
        default:
            textInsideBowl[0].style[property] = value;

    }
};

var controller = Leap.loop(controllerOptions, function (frame) {
     var i = 0;
    if (frame.pointables.length > 0) {
        var i=0;
        frame.pointables[i].length = 5000;
        var screenPosition = frame.pointables[i].tipPosition;
        cursor.show();
        
        logicOnObjects(cssColor, frame, i, function () {
            addProperty('color', 'aqua');
        }, function() {removeStyleFromObject('color', 'darkcyan')});
        logicOnObjects(cssFont, frame, i, function () {
            addProperty('font-size', '20px');
        }, function() {removeStyleFromObject('font-size', '20px')});
        logicOnObjects(cssAlign, frame, i, function () {
            addProperty('text-align', 'center');
        }, function() {removeStyleFromObject('text-align')});
        logicOnObjects(cssBold, frame, i, function () {
            addProperty('font-weight', 'bold');
        }, function() {removeStyleFromObject('font-weight')});
        logicOnObjects(cssItalic, frame, i, function () {
            addProperty('font-style', 'italic');
        }, function() {removeStyleFromObject('font-style')});
        logicOnObjects(cssBackgroundColor, frame, i, function () {
            addProperty('background-color', 'red');
        }, function() {removeStyleFromObject('background-color', 'white')});

        moveObject(cursor, screenPosition);
    }


}) .use('screenPosition', {
    scale: 1
})

$('#page-wrapper').mousedown(function(e) {
    var clicked = $(e.target); // get the element clicked
    if (clicked.is('.overlay') || clicked.parents().is('.overlay')) {
        return; // click happened within the dialog, do nothing here
    } else { // click was outside the dialog, so close it
        $('.overlay').addClass('hidden');
    }
});

