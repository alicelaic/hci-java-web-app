var instructions=
{
    'css': {'instruction': "Move your hand and try to move the elements and put them into the bowl.<br> " +
    "     When you are over an element you can see that the color is changed. In order to move an element you have to touch it.<br> " +
    "     Put it inside the bowl and you'll see what's happening"},
    'inheritance' : { 'instruction': "Make a pinch in the top-right corner of the stick-figure and after you grab it try to move it</br>. See what's happening when you make unpinch."},
    'tree' : {'instruction': "You'll see how <strong>BFS</strong> works. Make <strong>circles</strong> with your hand to see the traversing." }

};

$('.exampleButton').click(function(){

    var example=this.id;
    createExampleDiv(example);

    $('#exampleDiv').removeClass('hidden');
    $('#externalDiv').addClass('externalDiv');
    $('#exampleDiv').addClass('overlay');

    var exampleResource='<script src="/resources/js/'+example+'.js" />';

    $('body').append(exampleResource);
/*
    $('body').append('<script src="/resources/js/'+example+'.js />');
*/

});

$('.badgeButton').click(function(){
    $('#exampleDiv').removeClass('hidden');
    $('#externalDiv').addClass('externalDiv');
    $('#exampleDiv').addClass('overlay');
    getBadge();

});

var createExampleDiv = function (exampleName) {

    var explainDiv=$('<div  class="explainDiv"></div>');

    $('#canvas').remove();
    $('.explainDiv').remove();

    var explanationsParagraph=$('<p id="explanationsParagraph"></p>');
    explanationsParagraph.html(instructions[exampleName].instruction);
    explainDiv.append(explanationsParagraph);
    $('#exampleDiv').append(explainDiv);

    var canvasDiv=createCanvasDivFor(exampleName);

    $('#exampleDiv').append(canvasDiv);

};

function completeStep() {

    $('.explainDiv').remove();
    $('#canvas').remove();

    var explainDiv=$('<div  class="explainDiv"></div>');
    var explanationsParagraph=$('<p id="explanationsParagraph"></p>');
    explanationsParagraph.html("Put your medal in order to complete this step.");
    explainDiv.append(explanationsParagraph);

    var badgeImageDiv=$('<div class="badgeDiv"><img  style="width:100px;height:100px" src="/resources/img/exampleBadge.png"/></div>');
    var figureImageDiv=$('<div class="figureDiv"><img  class="figureAwarded" src="/resources/img/stick-figure.png" /></div>');
    var canvasDiv=$('<div id="canvas"> </div>');
    var cursorDiv=$('<div id="cursor"></div>');
    canvasDiv.append(badgeImageDiv);
    canvasDiv.append(figureImageDiv);
    canvasDiv.append(cursorDiv);
    $('#exampleDiv').append(explainDiv);
    $('#exampleDiv').append(canvasDiv);

    $("script[src='/resources/js/css.js']").remove();
    $("script[src='/resources/js/inheritance.js']").remove();
    $("script[src='/resources/js/tree.js']").remove();

    $('body').append('<script src="/resources/js/reward.js" />');

}

function getBadge() {
    var explainDiv=$('<div  class="explainDiv">Put your medal in order to complete this step.</div>');
    var badgeImageDiv=$('<div class="badgeDiv"><img  style="width:100px;height:100px" src="/resources/img/exampleBadge.png"/></div>');
    var figureImageDiv=$('<div class="figureDiv"><img  class="figureAwarded" src="/resources/img/stick-figure.png" /></div>');
    var canvasDiv=$('<div id="canvas"> </div>');
    var cursorDiv=$('<div id="cursor"></div>');
    canvasDiv.append(badgeImageDiv);
    canvasDiv.append(figureImageDiv);
    canvasDiv.append(cursorDiv);
    $('#exampleDiv').append(explainDiv);
    $('#exampleDiv').append(canvasDiv);

    $('body').append('<script src="/resources/js/reward.js" />');

}

function createCanvasDivFor(exampleName)
{
    var canvasDiv=$('<div id="canvas">');

    if(exampleName==='css') {
        return createBowlElementsFor(canvasDiv);
    }
    else if(exampleName==='inheritance') {
        return createInheritanceElementsFor(canvasDiv);
    }

    return canvasDiv;
}

function createBowlElementsFor(canvasDiv)
{
    var colorDiv=$('<div class="css-color css-element">Color</div>');
    var fontDiv=$('<div class="css-font css-element">Font</div>');
    var alignDiv=$('<div class="css-align css-element">Alignment</div>');
    var boldDiv=$(' <div class="css-bold css-element">Bold</div>');
    var italicDiv=$(' <div class="css-italic css-element">Italic</div>');
    var backgroundColorDiv=$('  <div class="css-background-color css-element">Background Color</div>');
    var bowlDiv=$(' <div class="bowl"><span id="text-inside-bowl">My beautiful text</span></div>');
    var cursorDiv=$('<div id="cursor"></div>');

    canvasDiv.append(colorDiv);
    canvasDiv.append(fontDiv);
    canvasDiv.append(alignDiv);
    canvasDiv.append(boldDiv);
    canvasDiv.append(italicDiv);
    canvasDiv.append(backgroundColorDiv);
    canvasDiv.append(bowlDiv);
    canvasDiv.append(cursorDiv);

    return canvasDiv;
}

function createInheritanceElementsFor(canvasDiv)
{
    var baseFigureImg=$('<div class="base-figure"><img class="base-figure-img" src="/resources/img/baseFigure.jpg"></div>');
    var leftChildImg=$('<div id="first-child"><img class="first-child-img" src="/resources/img/child.jpg"></div>');
    var rightChildImg=$('<div id="second-child"><img class="second-child-img" src="/resources/img/child.jpg"></div>');

    canvasDiv.append(baseFigureImg);
    canvasDiv.append(leftChildImg);
    canvasDiv.append(rightChildImg);

    return canvasDiv;
}

