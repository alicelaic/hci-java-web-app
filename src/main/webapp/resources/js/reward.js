window.cursor = $('#cursor');
window.output = $('#output');
window.bowl = $('.bowl');
window.canvas = $('#canvas');
window.explanationsParagraph = $('#explanationsParagraph');
window.exampleDiv = $('#exampleDiv');
window.canvasDiv = $('#canvas');
window.explainDiv = $('.explainDiv');

window.badgeDiv=$('.badgeDiv');
window.figureDiv=$('.figureDiv');

var hasTouched = false;


var touchLeft = function (objectLeft, objectWidth, cursorLeft, cursorWidth) {
    if ((cursorLeft + cursorWidth) >= objectLeft && cursorLeft <= (objectLeft + objectWidth)) {
        return true;
    }
    return false;
};

var touchTop = function (objectTop, objectHeight, cursorTop, cursorHeight) {
    if ((cursorTop + cursorHeight) >= objectTop && cursorTop <= (objectTop + objectHeight)) {
        return true;
    }
    return false;
};

/*the element movable in inside static content*/
var touchStaticObject = function (static, movable) {
    return touchLeft(static.offsetLeft, static.clientWidth, movable.offsetLeft, movable.clientWidth) &&
        touchTop(static.offsetTop, static.clientHeight, movable.offsetTop, movable.clientHeight);
};

var placeObjectInBowl = function (elem) {

    elemsInBowl[elem.selector] = true;

    if(elemsPlacedInBowl[elem.selector]==false)
    {
        numberOfElemPlacedInBowl++;
    }
    elemsPlacedInBowl[elem.selector]=true;

    elem.css({
        left: positionsInBowl[elem.selector].left + 'px',
        bottom: positionsInBowl[elem.selector].bottom - 200 + 'px',
        'opacity': '0.1'

    });
};


var moveObject = function (elem, screenPosition) {

    var leftPosition = screenPosition[0] * 5;
    var bottomPosition = screenPosition[1];

    if (leftPosition < canvas[0].offsetLeft) {
        leftPosition = canvas[0].offsetLeft;
    } else if (leftPosition >= canvas[0].offsetLeft + canvas[0].clientWidth) {
        leftPosition = canvas[0].offsetLeft + canvas[0].clientWidth;
    }

    if (bottomPosition > canvas[0].offsetTop + canvas[0].clientHeight) {
        bottomPosition = canvas[0].offsetTop + canvas[0].clientHeight;
    } else if (bottomPosition < canvas[0].offsetTop) {
        bottomPosition = canvas[0].offsetTop;
    }


    elem.css({
        left: leftPosition + 'px', /**5*/
        bottom: bottomPosition + 'px'
    });
};

var logicOnObjects = function (elem, frame, i) {
    var screenPosition = frame.pointables[i].tipPosition;

    if (touchStaticObject(elem[0], cursor[0]) && hasTouched === false) {


        if (frame.pointables[i].touchZone === 'touching') {
            console.log('touching');
            elem.css({
                'opacity': '0.5'
            });

            moveObject(elem, screenPosition);

            if ( touchStaticObject(figureDiv[0], elem[0])) {
                var leftPos = figureDiv[0].offsetLeft + 70 + 58 - 73;
                var bottomPos = figureDiv[0].offsetTop + figureDiv[0].clientHeight - 70 + 40 - 140;

                console.log(leftPos, bottomPos);
                elem.css({
                   left: leftPos + 'px',
                    bottom: bottomPos + 'px',
                    'z-index': 1298,
                    'opacity': 1
                });

                hasTouched = true;

                explanationsParagraph.html("You have a medal now! Close this and keep going with more medals!");

            }
        } else {

            elem.css({
                'opacity': '1'
            });

        }

    }
};


// Setup Leap loop with frame callback function
var controllerOptions = {enableGestures: true,background:false};

var controller = Leap.loop(controllerOptions, function (frame) {

    if (frame.pointables.length > 0) {
        var i = 1;
        frame.pointables[i].length = 5000;
        var screenPosition = frame.pointables[i].tipPosition;
        cursor.show();

        logicOnObjects(badgeDiv, frame, i);

        moveObject(cursor, screenPosition);
    }


}) .use('screenPosition', {
    scale: 1
})

$('#page-wrapper').mousedown(function(e) {
    var clicked = $(e.target); // get the element clicked
    if (clicked.is('.overlay') || clicked.parents().is('.overlay')) {
        return; // click happened within the dialog, do nothing here
    } else { // click was outside the dialog, so close it
        $('.overlay').addClass('hidden');
    }
});

