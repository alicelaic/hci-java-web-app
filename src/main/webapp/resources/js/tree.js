window.cursor = $('#cursor');
window.canvas = $('#canvas');
// Let's first initialize sigma:
var s = new sigma('canvas');
window.explanationsParagraph = $('#explanationsParagraph');
var colorNodes='#222222';

var finish=false;

// Then, let's add some data to display:
s.graph.addNode({
    // Main attributes:
    id: 'n0',
    label: 'Hello',
    // Display attributes:
    x: 0,
    y: 0,
    size: 20,
    color: colorNodes
}).addNode({
    // Main attributes:
    id: 'n01',
    label: 'World !',
    // Display attributes:
    x: -1,
    y: 0.5,
    size: 20,
    color:colorNodes
}).addEdge({
    id: 'e0',
    // Reference extremities:
    source: 'n0',
    target: 'n01'
}).addNode({
    // Main attributes:
    id: 'n1',
    label: 'World !',
    // Display attributes:
    x: 1,
    y: 0.5,
    size: 20,
    color:colorNodes
}).addEdge({
    id: 'e1',
    // Reference extremities:
    source: 'n0',
    target: 'n1'
}).addNode({
    // Main attributes:
    id: 'n21',
    label: 'World !',
    // Display attributes:
    x: -2,
    y: 1,
    size: 20,
    color:colorNodes
}).addEdge({
    id: 'e3',
    // Reference extremities:
    source: 'n01',
    target: 'n21'
}).addNode({
    // Main attributes:
    id: 'n22',
    label: 'World !',
    // Display attributes:
    x: 0,
    y: 1,
    size: 20,
    color:colorNodes
}).addEdge({
    id: 'e4',
    // Reference extremities:
    source: 'n1',
    target: 'n22'
}).addNode({
    // Main attributes:
    id: 'n23',
    label: 'World !',
    // Display attributes:
    x: 2,
    y: 1,
    size: 20,
    color:colorNodes
}).addEdge({
    id: 'e5',
    // Reference extremities:
    source: 'n1',
    target: 'n23'
}).addNode({
    // Main attributes:
    id: 'n31',
    label: 'World !',
    // Display attributes:
    x: -1,
    y: 1.5,
    size: 20,
    color:colorNodes
}).addEdge({
    id: 'e6',
    // Reference extremities:
    source: 'n21',
    target: 'n31'
}).addNode({
    // Main attributes:
    id: 'n32',
    label: 'World !',
    // Display attributes:
    x: 1,
    y: 1.5,
    size: 20,
    color:colorNodes
}).addEdge({
    id: 'e7',
    // Reference extremities:
    source: 'n23',
    target: 'n32'
});
/*    console.log(s.graph.nodes()[0]);
 s.graph.nodes()[0].x=100;
 console.log(s.graph.nodes()[0]);*/
// Finally, let's ask our sigma instance to refresh:
s.refresh();

var controllerOptions = {enableGestures: true};
var moveObject = function (elem, screenPosition) {
    var leftPosition = screenPosition[0] * 5;
    var bottomPosition = screenPosition[1];

    if (leftPosition < canvas[0].offsetLeft) {
        leftPosition = canvas[0].offsetLeft;
    } else if (leftPosition >= canvas[0].offsetLeft + canvas[0].clientWidth) {
        leftPosition = canvas[0].offsetLeft + canvas[0].clientWidth;
    }

    if (bottomPosition > canvas[0].offsetTop + canvas[0].clientHeight) {
        bottomPosition = canvas[0].offsetTop + canvas[0].clientHeight;
    } else if (bottomPosition < canvas[0].offsetTop) {
        bottomPosition = canvas[0].offsetTop;
    }

    elem.css({
        left: leftPosition + 'px', /**5*/
        bottom: bottomPosition + 'px'
    });
};
var index = 0;
var block = false;
var noOfNodes = s.graph.nodes().length;
console.log(s.graph.nodes(), s.graph.nodes().length, s.graph.nodes().size);
Leap.loop(controllerOptions, function (frame) {

    if (frame.pointables.length > 0) {
        var i = 1;
        frame.pointables[i].length = 5000;
        var screenPosition = frame.pointables[i].tipPosition;
        cursor.show();

        /*bowl.css({
         'background-color': 'white'
         });*/
        moveObject(cursor, screenPosition);
        if (index < noOfNodes) {
            frame.gestures.forEach(function (gesture) {
                switch (gesture.type) {
                    case "circle":
                        if (block === false) {
                            s.graph.nodes()[index].color = '#FF9999';
                            console.log(s.graph.nodes()[index].label);
                            s.graph.nodes()[index].label = index + 1 + "";
                            s.refresh();
                            console.log(index);
                            index++;
                            block = true;
                            setTimeout(function () {
                                block = false;
                            }, 2000);
                        }



                        break;
                }
            })
        }
        else {
            if (finish === false) {
                explanationsParagraph.append("<br><b>Congratulations! You have completed this example!</b>" + "Press the button to complete <button onClick='completeStep()'>Complete</button> ");
                finish=true;
            }

        }
        }


}) .use('screenPosition', {
    scale: 1
})