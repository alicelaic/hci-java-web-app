package demo;

/**
 * Created by Alice on 5/30/2015.
 */
public class Constants {
    public static final String CHAPTER_NAME_FILE = "domain{0}-chapter{1}.json";

    public static final String CHAPTER_BADGE_TYPE = "C";
    public static final String TEST_BADGE_TYPE = "T";
    public static final String EXAMPLE_BADGE_TYPE = "E";
    public static final String BOWL_EXAMPLE_BADGE = "CSS Master";
    public static final String CHECK_BADGE_AJAX_RESPONSE = "hasBadge";

    public static final String PREFIX_END_CHAPTER_BADGE_NAME = "Master in ";
}
