package demo.controller;

import demo.Constants;
import demo.domain.*;
import demo.domain.UIdomain.UIChapter;
import demo.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static demo.Constants.PREFIX_END_CHAPTER_BADGE_NAME;

@Controller
public class HomeController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @Autowired
    private TestRepository testRepository;

    @Autowired
    private DomainChapterRepository domainChapterRepository;

    @Autowired
    private UserBadgeRepository userBadgeRepository;


    @RequestMapping(value = {"/home"})
    public String viewStats(Map<String, Object> model) {
        return "home";
    }

    @RequestMapping(value = {"/"}, method = RequestMethod.GET)
    public String welcomePage(HttpSession session, Model model) throws IOException {

       List<Domain> domains = domainChapterRepository.getDomains();

        session.setAttribute("domains", domains);

        model.addAttribute("domains", domains);
        return "welcome";
    }

    @RequestMapping(value = {"/welcome"}, method = RequestMethod.POST)
    public String chooseDomainOfInterest(HttpServletRequest request, HttpSession session, Model model) throws IOException {
        String domainName = request.getParameter("domain");

        Domain domain = domainChapterRepository.getDomainByName(domainName);

        //check the progress of user to put him at that chapter
        Chapter activeChapter = domain.getChapters().get(0);

        session.setAttribute("activeChapter", activeChapter);

        return "redirect:/login";
    }

    @RequestMapping(value = {"/login"}, method = RequestMethod.GET)
    public String login() throws IOException {

       /* List<Domain> domains = domainChapterRepository.getDomains();
        domainChapterRepository.getParagraphForChapter(domains.get(0).getChapters().get(0));
       */
        return "login";
    }

    @RequestMapping(value = {"/userguide"}, method = RequestMethod.GET)
    public String userguide() throws IOException {

       /* List<Domain> domains = domainChapterRepository.getDomains();
        domainChapterRepository.getParagraphForChapter(domains.get(0).getChapters().get(0));
       */
        return "userguide";
    }


    @RequestMapping(value = {"/domain"}, method = RequestMethod.GET)
    public String domains(HttpSession session, @RequestParam(value = "name", defaultValue = "all") String domainName, Model model) throws IOException {

       /* List<Domain> domains = domainChapterRepository.getDomains();
        domainChapterRepository.getParagraphForChapter(domains.get(0).getChapters().get(0));
       */
        List<Domain> domains = domainChapterRepository.getDomains();
        Domain domain = domainChapterRepository.getDomainByName(domainName);
        // domainChapterRepository.getParagraphForChapter(domains.get(0).getChapters().get(0));

        //TODO get the chapter
        List<Chapter> chapters = domain.getChapters();

        session.setAttribute("domains", domains);
        //set domain
        session.setAttribute("domain", domain.getName());

        //set first chapter
        //TODO unde a ramas - bd (unde are progresul)
        session.setAttribute("chapter", chapters.get(0).getName());

        Chapter activeChapter = (Chapter) session.getAttribute("activeChapter");

        List<ChapterStatus> chapterStatuses = new ArrayList<>();
        boolean blocked = false;
        for(int i=0; i< chapters.size(); i++) {
            chapterStatuses.add(new ChapterStatus(chapters.get(i), blocked));
            if(chapters.get(i).getName().equalsIgnoreCase(activeChapter.getName())){
                blocked = true;
            }

        }

        //set all chapters to share along pages
        session.setAttribute("chapters", chapters);

        session.setAttribute("chaptersStatus", chapterStatuses);

        //operation type - learn or test
        session.setAttribute("operationType", "learn");


        //add to UI
        model.addAttribute("domains", domains);
        model.addAttribute("chapters", chapters);
        model.addAttribute("activeChapter", chapters.get(0));
        model.addAttribute("operationType", session.getAttribute("operationType"));
        model.addAttribute("chaptersStatus", session.getAttribute("chaptersStatus"));


        return "redirect:/chapter?name=" + chapters.get(0).getName();
    }

    @RequestMapping(value = "/chapter", method = RequestMethod.GET)
    public String chapters1(HttpServletRequest request, HttpSession session, @RequestParam("name") String chapterName, Model model) throws IOException {/*
        if(  SecurityContextHolder.getContext().getAuthentication() == null){
            return "redirect:/login";
        }*/
        List<ChapterStatus> chapterStatuses = (List<ChapterStatus>) session.getAttribute("chaptersStatus");
        Chapter currentActiveChapter = (Chapter) session.getAttribute("activeChapter");

        chapterName = checkIfChapterIsEnabled(chapterName, chapterStatuses, currentActiveChapter.getName());
        UIChapter uiChapter = domainChapterRepository.getParagraphForChapter(chapterName);

        Chapter chapter = domainChapterRepository.getChapterByName(chapterName);
        //change chapter
        session.setAttribute("chapter", chapterName);

        //set page number for chapter
        session.setAttribute("chapter-page", 0);

        //add paragraphs on session
        session.setAttribute("uichapter", uiChapter);

        //set active chapter
        session.setAttribute("activeChapter", chapter);

        //operation type - learn or test
        session.setAttribute("operationType", "learn");


        model.addAttribute("domains", session.getAttribute("domains"));
        model.addAttribute("chapters", session.getAttribute("chapters"));
        model.addAttribute("paragraphs", uiChapter.getPages().get(0).getParagraphs());
        model.addAttribute("activeChapter", chapter);
        model.addAttribute("operationType", session.getAttribute("operationType"));
        model.addAttribute("username", session.getAttribute("username"));
        model.addAttribute("chaptersStatus", chapterStatuses);

        return "chapter";
    }

    private String checkIfChapterIsEnabled(String chapterName, List<ChapterStatus> chapterStatuses, String newName) {
        for(ChapterStatus chapterStatus: chapterStatuses){
            if(chapterName.equalsIgnoreCase(chapterStatus.getChapter().getName()) && !chapterStatus.isBlocked()){
                return chapterName;
            }
        }

        return newName;
    }

    @RequestMapping(value = "/prev", method = RequestMethod.POST)
    public String prev(HttpSession session, Model model) throws IOException {

        String chapterName = (String) session.getAttribute("chapter");
        Integer currentPageNumber = (Integer) session.getAttribute("chapter-page");

        UIChapter uiChapter = (UIChapter) session.getAttribute("uichapter");

        Integer prevPageNumber;
        if (currentPageNumber == 0) {
            prevPageNumber = 0;
        } else {
            prevPageNumber = currentPageNumber - 1;
            session.setAttribute("chapter-page", prevPageNumber);
        }

        model.addAttribute("chapters", session.getAttribute("chapters"));
        model.addAttribute("paragraphs", uiChapter.getPages().get(prevPageNumber).getParagraphs());
        model.addAttribute("operationType", session.getAttribute("operationType"));
        model.addAttribute("username", session.getAttribute("username"));
        model.addAttribute("chaptersStatus", session.getAttribute("chaptersStatus"));

        return "chapter";
    }

    @RequestMapping(value = "/next", method = RequestMethod.POST)
    public String next(HttpSession session, Model model) throws IOException {
        String username = (String) session.getAttribute("username");
        model.addAttribute("username", username);

        String chapterName = (String) session.getAttribute("chapter");
        Integer currentPageNumber = (Integer) session.getAttribute("chapter-page");

        UIChapter uiChapter = (UIChapter) session.getAttribute("uichapter");

        Integer nextPageNumber;
        if (currentPageNumber == uiChapter.getMaxPages() - 1) {

            //add badge to profile
            userBadgeRepository.persistUserBadge(username, PREFIX_END_CHAPTER_BADGE_NAME + chapterName);

            //go to test section
            return "redirect:/test?name=" + chapterName;
        } else {
            nextPageNumber = currentPageNumber + 1;
            session.setAttribute("chapter-page", nextPageNumber);
        }


        model.addAttribute("chapters", session.getAttribute("chapters"));
        model.addAttribute("paragraphs", uiChapter.getPages().get(nextPageNumber).getParagraphs());
        model.addAttribute("operationType", session.getAttribute("operationType"));
        model.addAttribute("chaptersStatus", session.getAttribute("chaptersStatus"));
        return "chapter";
    }

    @RequestMapping(value = "/badge", method = RequestMethod.POST)
    public String addBadge(HttpServletRequest request, HttpSession session, Model model) throws IOException {
        // Chapter activeChapter = (Chapter) session.getAttribute("activeChapter");
        String badgeName = request.getParameter("badgeName");
        String username = (String) session.getAttribute("username");

        userBadgeRepository.persistUserBadge(username, badgeName);
        model.addAttribute("finish", true);

        return "chapter";
    }


    @RequestMapping(value = "/checkBadge", method = RequestMethod.GET)
    @ResponseBody
    public String checkBadge(HttpServletRequest request, HttpSession session, Model model) throws IOException {
        String badgeName = request.getParameter("badgeName");
        String username = (String) session.getAttribute("username");

        //TODO replace user
        if(userBadgeRepository.userHasBadge(username,badgeName))
        {
            return Constants.CHECK_BADGE_AJAX_RESPONSE;
        }

        return "";
    }

    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    public String profile(HttpSession session, Model model) throws IOException {
        String username = (String) session.getAttribute("username");
        List<ChapterStatus> chapterStatuses = (List<ChapterStatus>) session.getAttribute("chaptersStatus");

        model.addAttribute("badges", userBadgeRepository.getBadgesFor(username));
        model.addAttribute("username", username);
        model.addAttribute("progress", userRepository.getProgressForUser(username, chapterStatuses));
        return "profile";
    }

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public String test(HttpSession session, @RequestParam("name") String chapterName, Model model) throws IOException {
        UIChapter uiChapter = domainChapterRepository.getParagraphForChapter(chapterName);
        //change chapter
        Chapter activeChapter = (Chapter) session.getAttribute("activeChapter");
        Test test = testRepository.getTestForChapter(activeChapter);

        //set test on session
        session.setAttribute("test", test);

        //set question id on session
        session.setAttribute("questionId", 0);

        //set test as operation type
        session.setAttribute("operationType", "test");

        model.addAttribute("chapters", session.getAttribute("chapters"));
        model.addAttribute("paragraphs", uiChapter.getPages().get(0).getParagraphs());
        model.addAttribute("question", test.getQuestions().get(0));
        model.addAttribute("operationType", session.getAttribute("operationType"));
        model.addAttribute("username", session.getAttribute("username"));

        return "test";
    }

    @RequestMapping(value = "/submit-question", method = RequestMethod.POST)
    public String submitQuestion(HttpSession session, HttpServletRequest request, Model model) throws IOException {
        System.out.println(request.getParameter("answer"));
        Test test = (Test) session.getAttribute("test");
        String userName = (String) session.getAttribute("username");

        Integer questionId = (Integer) session.getAttribute("questionId");
        Integer answer = Integer.parseInt(request.getParameter("answer"));
        UserAuthenticate user = userRepository.findByEmail(userName);

        model.addAttribute("username", userName);

        testRepository.addAnswerOfUserInDB(user, test.getQuestions().get(questionId), answer);
        //send it to next question
        if (test.getQuestions().size() - 1 > questionId) {
            questionId = questionId + 1;

            //set question id on session
            session.setAttribute("questionId", questionId);
            model.addAttribute("question", test.getQuestions().get(questionId));
        } else {

            //add progress in db


            //see next chapter if exists

            Chapter activeChapter = (Chapter) session.getAttribute("activeChapter");
            List<Chapter> chapters = (List<Chapter>) session.getAttribute("chapters");

            Chapter nextChapter = getNextChapter(activeChapter, chapters);
            model.addAttribute("chapters", session.getAttribute("chapters"));
            if (nextChapter == null) {
                return "redirect:/profile";
            } else {
                List<ChapterStatus> chapterStatuses = (List<ChapterStatus>) session.getAttribute("chaptersStatus");
                for(ChapterStatus c: chapterStatuses){
                    if(c.getChapter().getName().equalsIgnoreCase(nextChapter.getName())){
                        c.setBlocked(false);
                    }
                }

                session.setAttribute("chaptersStatus", chapterStatuses);
                return "redirect:/chapter?name=" + nextChapter.getName();
            }
            //end of test

        }

        return "test";
    }

    private Chapter getNextChapter(Chapter activeChapter, List<Chapter> chapters) {
        int nextChapterPosition = 0;
        for (int i = 0; i < chapters.size(); i++) {
            if (chapters.get(i).getName().equals(activeChapter.getName()) && chapters.get(i).getDomain().getName().equals(activeChapter.getDomain().getName())) {
                nextChapterPosition = i + 1;
                break;
            }
        }

        if (nextChapterPosition == chapters.size()) {
            //there is not chapter available
            return null;
        } else {
            //return next chapter
            return chapters.get(nextChapterPosition);
        }

    }


    @RequestMapping(value = "/login", method = RequestMethod.POST)
      public String checkCredentials(HttpSession session, HttpServletRequest request, Model model) throws IOException {
        String email = request.getParameter("j_username");
        String passwd = request.getParameter("j_password");
        UserAuthenticate user = userRepository.findByEmail(email);

        //when error, add a param to know to show an error
        if (user == null) {
            return "redirect:/login";
        }
        if (!passwd.equals(user.getPassword())) {
            return "redirect:/login";
        }
        authenticateUserToApplication(user);
        session.setAttribute("username", user.getUsername());
        //check if there is an active domain, if not - return OOP
        Chapter chapter = (Chapter) session.getAttribute("activeChapter");
        String domainName = "Object Oriented Programming";

        if(chapter != null){
             domainName = chapter.getDomain().getName();
        }

        return domains(session, domainName, model);

    }
    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout(HttpSession session, HttpServletRequest request, Model model) throws IOException {
        session.invalidate();
        SecurityContextHolder.getContext().setAuthentication(null);

        return "redirect:/";

    }
    private void authenticateUserToApplication(UserAuthenticate newAccount) {
        UserDetails userDetails = userDetailsService.loadUserByUsername(newAccount
                .getUsername());
        Authentication auth = new UsernamePasswordAuthenticationToken(
                userDetails.getUsername(), userDetails.getPassword(),
                userDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(auth);
    }
}
