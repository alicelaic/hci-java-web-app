package demo.domain.UIdomain;

import java.util.List;

/**
 * Created by Alice on 5/30/2015.
 */
public class UIChapter {
    private String name;
    private Integer maxPages;
    private List<Page> pages;

    public UIChapter() {
        super();
    }

    public UIChapter(String name, List<Page> pages) {
        this.name = name;
        this.pages = pages;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Page> getPages() {
        return pages;
    }

    public void setPages(List<Page> pages) {
        this.pages = pages;
    }

    public Integer getMaxPages() {
        return maxPages;
    }

    public void setMaxPages(Integer maxPages) {
        this.maxPages = maxPages;
    }
}
