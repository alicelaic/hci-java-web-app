package demo.domain.UIdomain;

import java.util.List;

/**
 * Created by Alice on 5/30/2015.
 */
public class Page {
    private long id;
    private List<Paragraph> paragraphs;

    public Page(long id, List<Paragraph> paragraphs) {
        this.id = id;
        this.paragraphs = paragraphs;
    }

    public Page() {
        super();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<Paragraph> getParagraphs() {
        return paragraphs;
    }

    public void setParagraphs(List<Paragraph> paragraphs) {
        this.paragraphs = paragraphs;
    }
}
