package demo.domain.UIdomain;

/**
 * Created by Alice on 5/30/2015.
 */
public class Paragraph {
    private long number;
    private String description;

    public Paragraph(long number, String description) {
        this.number = number;
        this.description = description;
    }

    public Paragraph() {
        super();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getNumber() {
        return number;
    }

    public void setNumber(long number) {
        this.number = number;
    }
}
