package demo.domain;

/**
 * Created by Alice on 6/2/2015.
 */
public class ChapterStatus {
    private Chapter chapter;
    private boolean blocked;

    public ChapterStatus(Chapter chapter, boolean blocked) {
        this.chapter = chapter;
        this.blocked = blocked;
    }

    public ChapterStatus() {
        super();
    }

    public Chapter getChapter() {
        return chapter;
    }

    public void setChapter(Chapter chapter) {
        this.chapter = chapter;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }
}
