package demo.domain;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="progress")
public class Progress {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @OneToMany
    private List<Chapter> completedChapters;

    @OneToMany
    private List<Chapter> futureChapters;

    @OneToMany
    private List<UserAuthenticate> userAuthenticates;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<Chapter> getCompletedChapters() {
        return completedChapters;
    }

    public void setCompletedChapters(List<Chapter> completedChapters) {
        this.completedChapters = completedChapters;
    }

    public List<Chapter> getFutureChapters() {
        return futureChapters;
    }

    public void setFutureChapters(List<Chapter> futureChapters) {
        this.futureChapters = futureChapters;
    }

    public List<UserAuthenticate> getUserAuthenticates() {
        return userAuthenticates;
    }

    public void setUserAuthenticates(List<UserAuthenticate> userAuthenticates) {
        this.userAuthenticates = userAuthenticates;
    }
}
