package demo.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "user_badges")
@NamedQueries({
        @NamedQuery(name = UserBadge.GET_BADGES, query = "select u.badge from UserBadge u WHERE u.user= :user"),
        @NamedQuery(name = UserBadge.FIND_USER_BADGE, query = "select u.badge from UserBadge u WHERE u.badge.name= :badgeName")
})

public class UserBadge implements Serializable {

    public static final String GET_BADGES = "UserBadge.getBadges";
    public static final String FIND_USER_BADGE = "UserBadge.findUserBadge";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @JoinColumn(name = "id_badge")
    @ManyToOne
    private Badge badge;

    @JoinColumn(name = "id_user")
    @ManyToOne
    private UserAuthenticate user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Badge getBadge() {
        return badge;
    }

    public void setBadge(Badge badge) {
        this.badge = badge;
    }

    public UserAuthenticate getUser() {
        return user;
    }

    public void setUser(UserAuthenticate user) {
        this.user = user;
    }
}