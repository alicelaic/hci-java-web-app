package demo.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Alice on 5/28/2015.
 */

@Entity
@Table(name = "domain")
@NamedQueries({
        @NamedQuery(name="Domain.findAll", query="SELECT d FROM Domain d"),
        @NamedQuery(name="Domain.findByName", query="SELECT d FROM Domain d where d.name=:name")
})
public class Domain implements Serializable {

    public static final String findAll = "Domain.findAll";
    public static final String findByName = "Domain.findByName";
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "domain",  fetch = FetchType.EAGER)
    private List<Chapter> chapters;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Chapter> getChapters() {
        return chapters;
    }

    public void setChapters(List<Chapter> chapters) {
        this.chapters = chapters;
    }

    @Override
    public String toString() {
        return "Domain{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", chapters=" + chapters +
                '}';
    }
}
