package demo.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "badges")
@NamedQueries({
        @NamedQuery(name = Badge.FIND_BY_NAME, query = "from Badge u WHERE u.name = :name")
})
public class Badge {

    public static final String FIND_BY_NAME = "BadgeType.findByName";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @JoinColumn(name = "id_badge")
    @ManyToOne
    private BadgeType badgeType;

    @Column(name="name")
    private String name;

  /*  @ManyToMany(mappedBy="badges")
    private List<UserAuthenticate> users = new ArrayList<UserAuthenticate>();*/


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BadgeType getBadgeType() {
        return badgeType;
    }

    public void setBadgeType(BadgeType badgeType) {
        this.badgeType = badgeType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
