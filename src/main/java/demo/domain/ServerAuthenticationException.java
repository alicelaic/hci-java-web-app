
package demo.domain;

public class ServerAuthenticationException extends Exception {

    private String reason;

    public ServerAuthenticationException(String reason) {
        super(reason);
        this.reason = reason;
    }

    public String getReason() {
        return reason;
    }
}
