package demo.domain;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Alice on 5/28/2015.
 */


@Entity
@Table(name="chapter")
@NamedQueries({
        @NamedQuery(name="Chapter.findByName", query="SELECT c FROM Chapter c where c.name=:name")
})
public class Chapter implements Serializable {

    public static final String findByName = "Chapter.findByName";
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name="name")
    private String name;

    @ManyToOne
    private Domain domain;

    @OneToOne
    private Test test;

    public Domain getDomain() {
        return domain;
    }

    public void setDomain(Domain domain) {
        this.domain = domain;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Test getTest() {
        return test;
    }

    public void setTest(Test test) {
        this.test = test;
    }
}
