package demo.domain;

import javax.persistence.*;

/**
 * Created by Alice on 6/1/2015.
 */
@Entity
@Table(name = "user_answer")
public class UserAnswer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @ManyToOne
    private UserAuthenticate user;

    @ManyToOne
    private Question question;

    @Column
    private Integer answerProvided;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public UserAuthenticate getUser() {
        return user;
    }

    public void setUser(UserAuthenticate user) {
        this.user = user;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public Integer getAnswerProvided() {
        return answerProvided;
    }

    public void setAnswerProvided(Integer answerProvided) {
        this.answerProvided = answerProvided;
    }
}
