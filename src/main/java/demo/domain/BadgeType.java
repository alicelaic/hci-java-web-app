package demo.domain;

import javax.persistence.*;

@Entity
@Table(name = "badges_types")
@NamedQueries({
        @NamedQuery(name = BadgeType.FIND_BY_TYPE, query = "from BadgeType u WHERE u.type = :type")
})
public class BadgeType {

    public static final String FIND_BY_TYPE = "BadgeType.findByType";
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

/*from where he got the badge (E - example, T - test, C -chapter)*/

    @Column(name = "type")
    private String type;
    @Column(name = "source")
    private String source;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}

