package demo.domain;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Alice on 5/31/2015.
 */
@Entity
@Table(name = "tests")
@NamedQueries({
        @NamedQuery(name="Test.findByChapter", query="SELECT t FROM Test t WHERE t.chapter.id = :chapterId and t.chapter.domain.id = :domainId")
})
public class Test {
    public static final String findByChapter = "Test.findByChapter";
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @OneToOne
    private Chapter chapter;

    @OneToMany(fetch = FetchType.EAGER)
    private List<Question> questions;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Chapter getChapter() {
        return chapter;
    }

    public void setChapter(Chapter chapter) {
        this.chapter = chapter;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }
}
