package demo.domain;

import javax.persistence.*;
import java.util.Collections;
import java.util.List;

/**
 * Created by Alice on 12/3/2014.
 */
@Entity
@Table(name="user_auth")
@NamedQueries({
        @NamedQuery(name="UserAuthenticate.findByEmail", query="SELECT c FROM UserAuthenticate c where c.username=:email")
})
public class UserAuthenticate {
    public static final String findByEmail = "UserAuthenticate.findByEmail";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column
    private String username;

    @Column
    private String password;

    //change to set
    @ManyToMany (fetch = FetchType.LAZY)
    private List<Domain> domains;

   /* @ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(name = "user_badges",joinColumns = @JoinColumn(name = "id_user"),
            inverseJoinColumns = @JoinColumn(name = "id_badge")
    )
    private List<Badge> badges= Collections.emptyList();*/


    @ManyToOne
    private Progress progress;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<Domain> getDomains() {
        return domains;
    }

    public void setDomains(List<Domain> domains) {
        this.domains = domains;
    }

  /*  public List<Badge> getBadges() {
        return badges;
    }

    public void setBadges(List<Badge> badges) {
        this.badges = badges;
    }*/

    public Progress getProgress() {
        return progress;
    }

    public void setProgress(Progress progress) {
        this.progress = progress;
    }
}
