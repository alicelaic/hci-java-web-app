package demo.repository;

import demo.domain.UserAuthenticate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alice on 12/3/2014.
 */
@Service(value = "userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService{

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String code) throws UsernameNotFoundException {
        UserAuthenticate userAuthenticate = userRepository.findByEmail(code);

        List<GrantedAuthority> auth = new ArrayList<GrantedAuthority>();
        auth.add(new SimpleGrantedAuthority("ROLE_REG"));

        return new org.springframework.security.core.userdetails.User(code, userAuthenticate.getPassword(), true, true, true, true,
                auth);
    }
}
