package demo.repository;

import demo.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by Alice on 5/31/2015.
 */

@SuppressWarnings("unchecked")
@Service
@Transactional
public class TestRepository {

    @PersistenceContext
    private EntityManager em;

    public Test getTestForChapter(Chapter chapter){
        Test test = (Test) em.createNamedQuery(Test.findByChapter).setParameter("chapterId", chapter.getId()).setParameter("domainId", chapter.getDomain().getId()).getSingleResult();
        return test;
    }

    public void addAnswerOfUserInDB(UserAuthenticate user, Question question, Integer answer) {
        UserAnswer userAnswer = new UserAnswer();
        userAnswer.setQuestion(question);
        userAnswer.setUser(user);
        userAnswer.setAnswerProvided(answer);

        em.persist(userAnswer);
    }
}
