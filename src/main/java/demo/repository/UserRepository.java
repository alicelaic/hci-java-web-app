package demo.repository;

import demo.domain.ChapterStatus;
import demo.domain.UserAuthenticate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@SuppressWarnings("unchecked")
@Service
@Transactional
public class UserRepository{

    @PersistenceContext
    private EntityManager em;

    public UserAuthenticate findByEmail(String username) {
        UserAuthenticate user = null;

        List<UserAuthenticate> users = (List<UserAuthenticate>) em.createNamedQuery(UserAuthenticate.findByEmail).setParameter("email", username).getResultList();
        if(users.size() > 0){
            user = users.get(0);
        }
        return user;
    }

    public double getProgressForUser(String username, List<ChapterStatus> chapterStatuses) {
        int enabled = 0;
        int blocked = 0;
        for(ChapterStatus c: chapterStatuses) {
            if(c.isBlocked()){
                blocked++;
            }else{
                enabled++;
            }
        }

        return enabled*100/chapterStatuses.size();
    }

  /*  public void addProgress(String username){
        UserAuthenticate userAuthenticate = findByEmail(username);


        Progress ppro
        userAuthenticate.set
    }*/
}
