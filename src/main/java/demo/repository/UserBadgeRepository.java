package demo.repository;

import demo.domain.Badge;
import demo.domain.UserAuthenticate;
import demo.domain.UserBadge;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@SuppressWarnings("unchecked")
@Service
@Transactional
public class UserBadgeRepository {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BadgeRepository badgeRepository;

    public void persistUserBadge(String userEmail, String badgeName) {
        if (!userHasBadge(userEmail, badgeName)) {

            UserAuthenticate user = userRepository.findByEmail(userEmail);
            Badge badge = badgeRepository.getBadgeByName(badgeName);
            UserBadge userBadge = new UserBadge();
            userBadge.setBadge(badge);
            userBadge.setUser(user);

            em.persist(userBadge);
        }

    }

    public List<Badge> getBadgesFor(String userEmail) {
        UserAuthenticate user = userRepository.findByEmail(userEmail);

        List<Badge> badges = em.createNamedQuery(UserBadge.GET_BADGES).setParameter("user", user).getResultList();

        return badges;
    }

    public boolean userHasBadge(String userEmail, String badgeName) {
        UserAuthenticate user = userRepository.findByEmail(userEmail);

        List<Badge> badges = em.createNamedQuery(UserBadge.FIND_USER_BADGE).setParameter("badgeName", badgeName).getResultList();

        return badges.size() > 0;
    }

}
