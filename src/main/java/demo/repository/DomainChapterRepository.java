package demo.repository;

import demo.Constants;
import demo.domain.Chapter;
import demo.domain.Domain;
import demo.domain.UIdomain.UIChapter;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.File;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.List;

@Service
public class DomainChapterRepository {
    @PersistenceContext
    private EntityManager em;

    public List<Domain> getDomains(){
        List<Domain> domains = em.createNamedQuery(Domain.findAll).getResultList();

        for(Domain d: domains){
            System.out.println(d.toString());
        }
        return domains;
    }

    /*Get the chapters to be used in  UI later on*/
    public UIChapter getParagraphForChapter(Chapter chapter) throws IOException {
        String fileName = MessageFormat.format(Constants.CHAPTER_NAME_FILE, chapter.getDomain().getId(), chapter.getId());
        ObjectMapper mapper = new ObjectMapper();
        ClassLoader classLoader = getClass().getClassLoader();
        UIChapter uiChapter = mapper.readValue(new File(classLoader.getResource(fileName).getFile()), UIChapter.class);

        return uiChapter;
    }

    public Chapter getChapterByName(String chapterName) {
        List<Chapter> chapters = (List<Chapter>) em.createNamedQuery(Chapter.findByName).setParameter("name", chapterName).getResultList();
        return chapters.get(0);
    }

    public UIChapter getParagraphForChapter(String chapterName) throws IOException {
        Chapter chapter  = getChapterByName(chapterName);
        String fileName = MessageFormat.format(Constants.CHAPTER_NAME_FILE, chapter.getDomain().getId(), chapter.getId());
        ObjectMapper mapper = new ObjectMapper();
        ClassLoader classLoader = getClass().getClassLoader();
        UIChapter uiChapter = mapper.readValue(new File(classLoader.getResource(fileName).getFile()), UIChapter.class);

        return uiChapter;
    }


    public Domain getDomainByName(String domainName) {
        Domain domain = (Domain) em.createNamedQuery(Domain.findByName).setParameter("name", domainName).getSingleResult();
        return domain;
    }
}
