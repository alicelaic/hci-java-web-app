package demo.repository;

import demo.domain.Badge;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

@SuppressWarnings("unchecked")
@Service
@Transactional
public class BadgeRepository {

    @PersistenceContext
    private EntityManager em;

    public Badge getBadgeByName(String name){
        TypedQuery<Badge> query = (TypedQuery<Badge>) em.createNamedQuery(Badge.FIND_BY_NAME);
        query.setParameter("name", name);
        Badge badge;
        try{
            badge = query.getSingleResult();
            return badge;
        }catch(NoResultException e){
            return null;
        }
    }
}
